/**
  ******************************************************************************
  * @file    User/systick_delay.h 
  * @date    11-Dec-2017
  * @brief   all the functions prototypes for Systick
  ******************************************************************************
  */ 
 
#ifndef __SYSTICK_DELAY_H
#define __SYSTICK_DELAY_H

#include "stm32f10x.h"

#define delay_ms(x) Delay_us(1000*x)
#define delay_us(x) Delay_us(x)

void Systick_Delay_Init(void);
void Delay_us(uint32_t delayedTime);

#endif
