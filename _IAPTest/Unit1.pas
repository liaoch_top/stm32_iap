unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPComm, StdCtrls, Registry, StrUtils, jpeg, ExtCtrls;

type
  TForm1 = class(TForm)
    btn1: TButton;
    btn2: TButton;
    cm1: TComm;
    edt1: TEdit;
    btn3: TButton;
    mmo1: TMemo;
    cbb1: TComboBox;
    cbb2: TComboBox;
    lbl1: TLabel;
    lbl2: TLabel;
    dlgOpen1: TOpenDialog;
    btn4: TButton;
    btn5: TButton;
    edt2: TEdit;
    lbl3: TLabel;
    cbb3: TComboBox;
    edt3: TEdit;
    lbl4: TLabel;
    img1: TImage;
    procedure btn2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure cm1ReceiveData(Sender: TObject; Buffer: Pointer; BufferLength: Word);
    procedure SearchCOM(Sender: TObject);
    procedure cbb1Change(Sender: TObject);
    procedure cbb2Change(Sender: TObject);
    procedure cbb1DropDown(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure SendFrame(Sender: TObject; pAll: Byte; pCnt: Byte; str: string);
    procedure cbb3Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  foFlag: Boolean;
  recBuff: string;
  binStr: string;
  dlFlag: Boolean;

implementation

{$R *.dfm}

procedure TForm1.btn2Click(Sender: TObject);
begin
  cm1.StopComm;
  cbb1.Enabled := True;
  cbb2.Enabled := True;
  cbb3.Enabled := True;
  btn1.Enabled := True;
  btn2.Enabled := False;
  btn3.Enabled := False;
//  btn4.Enabled := False;
  btn5.Enabled := False;
end;

procedure TForm1.btn1Click(Sender: TObject);
begin
  cm1.StopComm;
  cm1.CommName := '//./' + cbb1.Items[cbb1.ItemIndex];
  cm1.BaudRate := Cardinal(StrToInt(cbb2.Items[cbb2.ItemIndex]));
//  ShowMessage(cm1.CommName);
//  ShowMessage(IntToStr(cm1.BaudRate));
  // 起始位为1位，逻辑0   //不可设置
  cm1.ByteSize := _8;     // 数据位8位
  cm1.Parity := Tparity(cbb3.ItemIndex); // 校验方式偶校验
//  cm1.ParityCheck := False;
  cm1.StopBits := _1;     // 停止位1位
  try
    begin
      cm1.StartComm;
      cbb1.Enabled := False;
      cbb2.Enabled := False;
      cbb3.Enabled := False;
      btn1.Enabled := False;
      btn2.Enabled := True;
      btn3.Enabled := True;
//      btn4.Enabled := True;
      btn5.Enabled := True;
    end
  except
    begin
      ShowMessage('Open COM error!');
    end;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  cbb1.Style := csDropDownList;
  cbb2.Style := csDropDownList;
  cbb3.Style := csDropDownList;
  cbb2.ItemIndex := 0;
  cbb3.ItemIndex := 0;
  btn1.Enabled := False;
  btn2.Enabled := False;
  btn3.Enabled := False;
//  btn4.Enabled := False;
  btn5.Enabled := False;
  edt1.Clear;
  edt2.Clear;
  edt3.Text := IntToStr(254);
  mmo1.Clear;

  foFlag := False;
  recBuff := '';
  binStr := '';
  dlFlag := False;
end;

procedure TForm1.btn3Click(Sender: TObject);
begin
  cm1.WriteCommData(PChar(edt1.Text), Length(edt1.Text));
end;

procedure TForm1.cm1ReceiveData(Sender: TObject; Buffer: Pointer; BufferLength: Word);
begin
  recBuff := PChar(Buffer);
  if mmo1.Lines.Count > 100 then
  begin
    mmo1.Clear;
  end;
  mmo1.Lines.Add('recBuff:' + recBuff);
end;

procedure TForm1.SearchCOM(Sender: TObject);
var
  reg: TRegistry;
  ts: TStrings;
  cnt: Integer;
  str: string;
begin
  cbb1.Items.Clear;
  reg := TRegistry.Create;
  reg.RootKey := HKEY_LOCAL_MACHINE;
  reg.OpenKey('hardware\devicemap\serialcomm', False);
  ts := TStringList.Create;
  reg.GetValueNames(ts);
  for cnt := 0 to ts.Count - 1 do
  begin
    str := ts[cnt];
    if Pos('Serial', str) > 0 then
    begin
      cbb1.Items.Add(reg.ReadString(str));
    end;
  end;
  cbb1.ItemIndex := 0;
  ts.Free;
  reg.CloseKey;
  reg.Free;
end;

procedure TForm1.cbb1Change(Sender: TObject);
begin
  cm1.CommName := '//./' + cbb1.Items[cbb1.ItemIndex];
end;

procedure TForm1.cbb2Change(Sender: TObject);
begin
  cm1.BaudRate := Cardinal(StrToInt(cbb2.Items[cbb2.ItemIndex]));
end;

procedure TForm1.cbb1DropDown(Sender: TObject);
begin
  SearchCOM(nil);
  if cbb1.Items[0] <> '' then
  begin
    btn1.Enabled := True;
  end
  else
  begin
    ShowMessage('No COM!');
  end;
end;

procedure TForm1.btn4Click(Sender: TObject);
begin
//  dlgOpen1.Filter := '程序文件(*.hex)|*.hex';
  dlgOpen1.Filter := '二进制文件(*.bin)|*.bin';
  if dlgOpen1.Execute then
  begin
    foFlag := True;
    edt2.Text := dlgOpen1.FileName;
  end;
end;

procedure TForm1.btn5Click(Sender: TObject);
var
  remain: LongWord;
  dLen, pAll, pCnt: Byte;
  tempInt: Integer;
  myStream: TMemoryStream;
  tempStr: string;
begin
  if foFlag then
  begin
    dlFlag := not dlFlag;
    if dlFlag then
    begin
      btn5.Caption := 'Stop';
      myStream := TMemoryStream.Create;
      myStream.LoadFromFile(edt2.Text);
      SetLength(binStr, myStream.Size);
      myStream.ReadBuffer((PChar(binStr))^, myStream.Size);
      myStream.Free;

      remain := Length(binStr);
      mmo1.Lines.Add('remain:' + IntToStr(remain));
      if remain > 0 then
      begin
        tempInt := StrToInt(edt3.Text);
        if tempInt < 32 then
        begin
          tempInt := 32;
        end
        else if tempInt > 255 then
        begin
          tempInt := 252;
        end
        else
        begin
          tempInt := tempInt div 4 * 4;
        end;
        dLen := tempInt and 255;
        edt3.Text := IntToStr(dLen);
        pCnt := 0;
        pAll := remain div dLen;
        if (remain mod dLen) <> 0 then
        begin
          pAll := pAll + 1;
        end;
        while (remain > dLen) and dlFlag do
        begin
          tempStr := Copy(binStr, pCnt * dLen + 1, dLen);
          SendFrame(nil, pAll, pCnt, tempStr);
          // wait for ack
          while (Length(recBuff) < 1) and dlFlag do
          begin
            Application.ProcessMessages;
          end;
          if Length(recBuff) = 1 then
          begin
            if recBuff[1] = Char(238) then   // 0xEE
            begin
              pCnt := pCnt + 1;
              remain := remain - dLen;
            end;
          end;
          recBuff := '';
        end;
        mmo1.Lines.Add('remain:' + IntToStr(remain));
        while (remain > 0) and dlFlag do
        begin
          tempStr := Copy(binStr, pCnt * dLen + 1, dLen);
          SendFrame(nil, pAll, pCnt, tempStr);
          // wait for ack
          while (Length(recBuff) < 1) and dlFlag do
          begin
            Application.ProcessMessages;
          end;
          if Length(recBuff) = 1 then
          begin
            if recBuff[1] = Char(238) then   // 0xEE
            begin
              pCnt := 0;
              remain := 0;
            end;
          end;
          recBuff := '';
        end;
      end
      else
      begin
        ShowMessage('No content!');
      end;
      dlFlag := False;
      btn5.Caption := 'Download';
    end
    else
    begin
      btn5.Caption := 'Download';
    end;
  end;
end;

procedure TForm1.SendFrame(Sender: TObject; pAll: Byte; pCnt: Byte; str: string);
var
  len, cnt, bcnt, tmp: Byte;
  sendStr: string;
  evenFlag: Boolean;
begin
  sendStr := '';
  len := Length(str);
  mmo1.Lines.Add('len:' + IntToStr(len));
  sendStr := sendStr + Char(238);    // 0xEE
  sendStr := sendStr + Char(pAll);       // package amount
  sendStr := sendStr + Char(pCnt);   // package number
  sendStr := sendStr + Char(len);    // length
  sendStr := sendStr + str;          // data
  evenFlag := False;
  for cnt := 1 to len do
  begin
    tmp := Byte(str[cnt]);
    for bcnt := 0 to 7 do
    begin
      if tmp and 1 > 0 then
      begin
        evenFlag := not evenFlag; // 帧偶校验
      end;
      tmp := tmp shr 1;
    end;
  end;
  if evenFlag then
  begin
    tmp := 1;
  end
  else
  begin
    tmp := 0;
  end;
  sendStr := sendStr + Char(tmp); // checksum
  cm1.WriteCommData(PChar(sendStr), Length(sendStr));
end;

procedure TForm1.cbb3Change(Sender: TObject);
begin
  cm1.Parity := Tparity(cbb3.ItemIndex);
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dlFlag := False;
end;

end.
